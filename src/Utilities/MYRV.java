/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Utilities;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Shape;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Path2D;
import java.awt.geom.RoundRectangle2D;
import javax.swing.JFrame;
import javax.swing.border.LineBorder;

/**
 *
 * @author MYRV
 */
public class MYRV {
    public interface Callback { public void run(); }
    public static class MFrame extends javax.swing.JFrame {
        public static Color BColor  = new Color(128, 128, 128);
        public static Float BRadius = 14f;
        
        public MFrame() {
            setUndecorated(true);
            var frameDragListener = new FrameDragger(this);
            addMouseListener(frameDragListener);
            addMouseMotionListener(frameDragListener);
        }
        
        public void superPostInitialize() {
            setShape(new RoundRectangle2D.Double(0, 0, getWidth(), getHeight(), BRadius, BRadius));
            getRootPane().setBorder(new FrameBorder(BColor, 0, BRadius));
            setLocationRelativeTo(null);
        }
        
        private static class FrameDragger extends MouseAdapter {
            private final JFrame frame;
            private Point mouseCoords = null;
            public FrameDragger(JFrame frame) { this.frame = frame; }

            @Override public void mouseReleased(MouseEvent e){ mouseCoords = null; }
            @Override public void mousePressed(MouseEvent e) { mouseCoords = e.getPoint(); }
            @Override public void mouseDragged(MouseEvent e) {
                if (mouseCoords.y > 30) return;
                Point currCoords = e.getLocationOnScreen();
                frame.setLocation(currCoords.x - mouseCoords.x, currCoords.y - mouseCoords.y);
            }
        }
    
        public static class FrameBorder extends LineBorder {
            public FrameBorder(Color color, int thickness, float radius) { super(color, thickness, true); }
            @Override public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
                if (!(this.thickness > 0) || !(g instanceof Graphics2D)) return;
                Graphics2D g2d = (Graphics2D) g;
                Color oldColor = g2d.getColor();
                g2d.setColor(FrameBorder.this.lineColor);
                Shape outer, inner;
                int offs = FrameBorder.this.thickness;
                int size = offs + offs;
                //
                outer = new RoundRectangle2D.Double(x, y, width, height, BRadius, BRadius);
                inner = new RoundRectangle2D.Double(x + offs, y + offs, width - size, height - size, BRadius, BRadius);
                //
                Path2D path = new Path2D.Float(Path2D.WIND_NON_ZERO);
                path.append(outer, true);
                path.append(inner, true);
                g2d.fill(path);
                g2d.setColor(oldColor);
            }
        }
        
        
        public static void serve(JFrame frame) {
            try {
                for (var info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                    if (!"Nimbus".equals(info.getName())) continue;
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
                java.util.logging.Logger.getLogger(MFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            }
            frame.setVisible(true);
        }
    }
}
