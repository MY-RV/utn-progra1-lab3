/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Utilities;

import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 *
 * @author MYRV
 */
public class Valid {
    public static Integer Int(JTextField Field) {
        return Int(Field.getText());
    }
    public static Integer Int(JLabel Field) {
        return Int(Field.getText());
    }
    public static Integer Int(String text) {
        try {
            return Integer.valueOf(text);
        } catch (NumberFormatException e) {
            return null;
        }
    }
}
