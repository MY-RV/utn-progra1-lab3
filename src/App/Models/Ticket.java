/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Record.java to edit this template
 */
package App.Models;

/**
 *
 * @author MYRV
 */

public class Ticket {
    public Description  desc;
    public Integer      amount;
    public Ticket(int amount, Description desc) {
        this.amount = amount;
        this.desc   = desc;
    }
    
    public static record Description( /** Bad Application, just Experimental **/
        String  location, 
        Integer price,
        String  sponsor
    ) { }
}
