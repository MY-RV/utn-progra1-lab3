/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package App.Models;

import App.Database.DQ;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

/**
 *
 * @author MYRV
 */
public record UserTickets(
    Integer user_id,
    String  location,
    Integer amount,
    Date    created_at
) {
    private static final SimpleDateFormat FDate = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    
    public Ticket ticket() {
        return DQ.Tickets.find((ticket) -> location.equals(ticket.desc.location()));
    }
    
    public User user() {
        return DQ.Users.find((row) -> Objects.equals(user_id, row.id()));
    }
    
    public String date() { return FDate.format(this.created_at); }
    
}
