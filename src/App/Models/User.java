/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package App.Models;

/**
 *
 * @author MYRV
 */
public record User(
    Integer id,
    String  password,
    String  name,
    String  gender,
    String  role
) { }
    