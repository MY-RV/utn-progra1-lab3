/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package App.Database;

import App.Models.Ticket;
import App.Models.User;
import App.Models.UserTickets;
import java.util.ArrayList;

/**
 *
 * @author MYRV
 */
public class DB {
    public static ArrayList<Ticket> Tickets = new ArrayList<>();
    public static ArrayList<UserTickets> UsersTickets = new ArrayList<>();
    public static User Auth;
    
    public static ArrayList<User> Users = new ArrayList<>(){{
        add(new User(12345, "", "Admin", "", "admin"));
    }};
    
    public static ArrayList<String> Localities = new ArrayList<>(){{
        add("Sombra Este"); add("Sombra Oeste"); add("Sol Sur"); add("Sol Norte"); add("Palco Este");
    }};
    
    public static ArrayList<String> Sponsors = new ArrayList<>(){{
        add("BAC Credomatic"); add("Scotiabank"); add("Banco Nacional"); add("Grupo Q"); add("Coca Cola");
    }};
    
}
