/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package App.Database.Queries;

import App.Database.DB;
import App.Models.Ticket;
import java.util.ArrayList;

/**
 *
 * @author MYRV
 */
public class QTickets {
    public interface Condition { public boolean run(Ticket ticket); }
    
    public Ticket find(Condition con) {
        for (var ticket : DB.Tickets)
            if (con.run(ticket)) return ticket;
        return null;
    }
    
    public Integer getIndex(Condition con) {
        for (int i = 0; i < DB.Tickets.size(); i++) {
            var ticket = DB.Tickets.get(i);
            if (con.run(ticket)) return i;
        }
        return null;
    }
    
    public ArrayList<Ticket> where(Condition con) {
        ArrayList<Ticket> response = new ArrayList<>();
        for (var relation : DB.Tickets) 
            if (con.run(relation)) response.add(relation);
        return response;
    }
}
