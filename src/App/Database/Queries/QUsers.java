/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package App.Database.Queries;

import App.Database.DB;
import App.Models.User;
import java.util.ArrayList;

/**
 *
 * @author MYRV
 */
public class QUsers {
    public interface Condition { public boolean run(User user); }

    public void insert(User user) {
        DB.Users.add(user);
    }
    
    public User find(Condition con) {
        for (var user : DB.Users)
            if (con.run(user)) return user;
        return null;
    }
    
    public ArrayList<User> where(Condition con) {
        ArrayList<User> response = new ArrayList<>();
        for (var row : DB.Users) 
            if (con.run(row)) response.add(row);
        return response;
    }
}

