/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package App.Database.Queries;

import App.Database.DB;
import App.Models.UserTickets;
import java.util.ArrayList;

/**
 *
 * @author MYRV
 */
public class QUserTickets {
    public interface Condition { public boolean run(UserTickets relation); }
    
    public UserTickets find(Condition con) {
        for (var relation : DB.UsersTickets) 
            if (con.run(relation)) return relation;
        return null;
    }
    
    public ArrayList<UserTickets> where(Condition con) {
        ArrayList<UserTickets> response = new ArrayList<>();
        for (var relation : DB.UsersTickets) 
            if (con.run(relation)) response.add(relation);
        return response;
    }

}
