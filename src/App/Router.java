/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package App;

import javax.swing.JFrame;

/**
 *
 * @author MYRV
 */
public class Router {
    private static final JFrame[] Views = new JFrame[]{
        new App.Views.Auth.SingIn(),
        new App.Views.Auth.SingUp(),
        new App.Views.TicketsBuyer(),
        new App.Views.TicketsForm(),
        new App.Views.Reports(),
    };
    
    public static void serve() { goTo("Auth.SingIn"); }
    
    public static void goTo(String name) {
        for (var view : Views) {
            var viewName = view.getClass().getName().replace("App.Views.","");
            view.setVisible(name.equals(viewName));
        }
    }
}
