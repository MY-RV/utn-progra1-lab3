/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package App.Views;

import App.Database.DB;
import App.Database.DQ;
import App.Models.Ticket;
import App.Router;
import Utilities.MYRV;
import Utilities.Valid;
import java.awt.Color;

/**
 *
 * @author MYRV
 */
public class TicketsForm extends MYRV.MFrame {

    private boolean Loaded = false;
    // public static void main(String[] args) { serve(new TicketsForm()); }
    public TicketsForm() {
        super(); initComponents();
        superPostInitialize();
        Form.setOpaque(false);
        Form.setBorder(new FrameBorder(new Color(250, 250, 250), 400, 20f));
        Shadow.setOpaque(false);
        Shadow.setBorder(new FrameBorder(new Color(240, 240, 240), 400, 20f));
    }
    
    @Override 
    public void setVisible(boolean visible) {
        if (visible) {
            this.Loaded = false;
            CbxLocation.removeAllItems();
            CbxSponsor.removeAllItems();
            for (var row : DB.Localities) CbxLocation.addItem(row);
            CbxLocation.setSelectedIndex(-1);
            for (var row : DB.Sponsors) CbxSponsor.addItem(row);
            this.Loaded = true;
            getCurrentState();
        }
        super.setVisible(visible);
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        ActBtnClose = new javax.swing.JButton();
        ActBtnLogout = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        SystemReports = new javax.swing.JButton();
        Form = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        CbxLocation = new javax.swing.JComboBox<>();
        TxtPrice = new javax.swing.JTextField();
        SpnAmount = new javax.swing.JSpinner();
        CbxSponsor = new javax.swing.JComboBox<>();
        BtnSave = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        Shadow = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        ActBtnClose.setBackground(new java.awt.Color(255, 51, 51));
        ActBtnClose.setFont(new java.awt.Font("Font Awesome 6 Free Solid", 0, 20)); // NOI18N
        ActBtnClose.setForeground(new java.awt.Color(255, 255, 255));
        ActBtnClose.setText("\uf00d");
        ActBtnClose.setBorderPainted(false);
        ActBtnClose.setContentAreaFilled(false);
        ActBtnClose.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        ActBtnClose.setFocusPainted(false);
        ActBtnClose.setMargin(new java.awt.Insets(0, 0, 0, 0));
        ActBtnClose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ActBtnCloseActionPerformed(evt);
            }
        });
        jPanel1.add(ActBtnClose, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 0, 30, 30));

        ActBtnLogout.setBackground(new java.awt.Color(255, 51, 51));
        ActBtnLogout.setFont(new java.awt.Font("Font Awesome 6 Free Solid", 0, 20)); // NOI18N
        ActBtnLogout.setForeground(new java.awt.Color(255, 255, 255));
        ActBtnLogout.setText("\uf08b");
        ActBtnLogout.setToolTipText("Logout");
        ActBtnLogout.setBorderPainted(false);
        ActBtnLogout.setContentAreaFilled(false);
        ActBtnLogout.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        ActBtnLogout.setFocusPainted(false);
        ActBtnLogout.setMargin(new java.awt.Insets(0, 0, 0, 0));
        ActBtnLogout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ActBtnLogoutActionPerformed(evt);
            }
        });
        jPanel1.add(ActBtnLogout, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 0, 30, 30));

        jPanel2.setBackground(new java.awt.Color(51, 51, 51));
        jPanel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel2.setForeground(new java.awt.Color(51, 51, 51));
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        SystemReports.setBackground(new java.awt.Color(51, 51, 51));
        SystemReports.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        SystemReports.setForeground(new java.awt.Color(204, 204, 204));
        SystemReports.setText("Reportes");
        SystemReports.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        SystemReports.setFocusPainted(false);
        SystemReports.setFocusable(false);
        SystemReports.setMargin(new java.awt.Insets(0, 0, 0, 0));
        SystemReports.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SystemReportsActionPerformed(evt);
            }
        });
        jPanel2.add(SystemReports, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 80, 30));

        jPanel1.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 400, 30));

        Form.setBackground(new java.awt.Color(255, 255, 255));
        Form.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel5.setFont(new java.awt.Font("Font Awesome 6 Free Solid", 0, 18)); // NOI18N
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("\ue140");
        Form.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 130, 30, 30));

        CbxLocation.setBackground(new java.awt.Color(255, 254, 255));
        CbxLocation.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        CbxLocation.setToolTipText("");
        CbxLocation.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CbxLocationActionPerformed(evt);
            }
        });
        Form.add(CbxLocation, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 30, 120, -1));

        TxtPrice.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        TxtPrice.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TxtPrice.setText("Precio");
        TxtPrice.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                TxtPriceInputMethodTextChanged(evt);
            }
        });
        TxtPrice.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TxtPriceActionPerformed(evt);
            }
        });
        TxtPrice.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                TxtPriceKeyTyped(evt);
            }
        });
        Form.add(TxtPrice, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 130, 120, 30));

        SpnAmount.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        SpnAmount.setModel(new javax.swing.SpinnerNumberModel(0, 0, null, 1));
        SpnAmount.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                SpnAmountStateChanged(evt);
            }
        });
        SpnAmount.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                SpnAmountInputMethodTextChanged(evt);
            }
        });
        SpnAmount.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                SpnAmountKeyTyped(evt);
            }
        });
        Form.add(SpnAmount, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 180, 120, -1));

        CbxSponsor.setBackground(new java.awt.Color(255, 254, 255));
        CbxSponsor.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        Form.add(CbxSponsor, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 80, 120, 30));

        BtnSave.setBackground(new java.awt.Color(255, 254, 255));
        BtnSave.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        BtnSave.setText("Guardar");
        BtnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSaveActionPerformed(evt);
            }
        });
        Form.add(BtnSave, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 230, 130, 30));

        jLabel2.setFont(new java.awt.Font("Comic Sans MS", 1, 18)); // NOI18N
        jLabel2.setText("Disponibilidad");
        Form.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 180, -1, -1));

        jLabel3.setFont(new java.awt.Font("Comic Sans MS", 1, 18)); // NOI18N
        jLabel3.setText("Localización");
        Form.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 30, -1, -1));

        jLabel4.setFont(new java.awt.Font("Comic Sans MS", 1, 18)); // NOI18N
        jLabel4.setText("Patrocinador");
        Form.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 80, -1, -1));

        jLabel6.setFont(new java.awt.Font("Comic Sans MS", 1, 18)); // NOI18N
        jLabel6.setText("Precio");
        Form.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 130, -1, -1));

        jPanel1.add(Form, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 50, 310, 280));

        javax.swing.GroupLayout ShadowLayout = new javax.swing.GroupLayout(Shadow);
        Shadow.setLayout(ShadowLayout);
        ShadowLayout.setHorizontalGroup(
            ShadowLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 310, Short.MAX_VALUE)
        );
        ShadowLayout.setVerticalGroup(
            ShadowLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 280, Short.MAX_VALUE)
        );

        jPanel1.add(Shadow, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 80, 310, 280));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Graphics/bgPurple.jpg"))); // NOI18N
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 400, 390));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 400, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void CbxLocationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CbxLocationActionPerformed
        getCurrentState();
    }//GEN-LAST:event_CbxLocationActionPerformed
    private void TxtPriceInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_TxtPriceInputMethodTextChanged
        validateInputs();
    }//GEN-LAST:event_TxtPriceInputMethodTextChanged
    private void SpnAmountInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_SpnAmountInputMethodTextChanged
        validateInputs();
    }//GEN-LAST:event_SpnAmountInputMethodTextChanged
    private void BtnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSaveActionPerformed
        saveRecord();
    }//GEN-LAST:event_BtnSaveActionPerformed
    private void TxtPriceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TxtPriceActionPerformed
        validateInputs();
    }//GEN-LAST:event_TxtPriceActionPerformed
    private void SpnAmountStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_SpnAmountStateChanged
        validateInputs();
    }//GEN-LAST:event_SpnAmountStateChanged
    private void TxtPriceKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtPriceKeyTyped
        validateInputs();
    }//GEN-LAST:event_TxtPriceKeyTyped
    private void SpnAmountKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_SpnAmountKeyTyped
        validateInputs();
    }//GEN-LAST:event_SpnAmountKeyTyped

    private void ActBtnLogoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ActBtnLogoutActionPerformed
        DB.Auth = null;
        Router.goTo("Auth.SingIn");
    }//GEN-LAST:event_ActBtnLogoutActionPerformed

    private void ActBtnCloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ActBtnCloseActionPerformed
        System.exit(0);
    }//GEN-LAST:event_ActBtnCloseActionPerformed

    private void SystemReportsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SystemReportsActionPerformed
        Router.goTo("Reports");
    }//GEN-LAST:event_SystemReportsActionPerformed

    private void getCurrentState() {
        if (!this.Loaded) return;
        var index = CbxLocation.getSelectedIndex();
        if (index == -1) {
            resetInputs();
            return;
        }
        var location = DB.Localities.get(index);
        var ticket = DQ.Tickets.find((row) -> row.desc.location().equals(location));
        if (ticket == null) {
            resetInputs();
            return;
        }
        index = 0;
        for (var row : DB.Sponsors) {
            if (ticket.desc.sponsor().equals(row)) {
                CbxSponsor.setSelectedIndex(index);
                break;
            } else index++;
        }
        TxtPrice.setText(String.valueOf(ticket.desc.price()));
        SpnAmount.setValue(ticket.amount);
        BtnSave.setText("Actualizar");
        CbxSponsor.setEnabled(false);
        TxtPrice.setEnabled(false);
        BtnSave.setEnabled(false);
    }
    
    private void resetInputs() {
        CbxSponsor.setSelectedIndex(-1);
        TxtPrice.setText("0");
        SpnAmount.setValue(0);
        BtnSave.setText("Guardar");
        CbxSponsor.setEnabled(true);
        TxtPrice.setEnabled(true);
        BtnSave.setEnabled(false);
    }
    
    private boolean validPrice() {
        var value = Valid.Int(TxtPrice);
        if (value == null) {
            TxtPrice.setForeground(Color.RED);
            return false;
        } else if(value < 0) {
            TxtPrice.setForeground(Color.RED);
            return false;
        } else TxtPrice.setForeground(Color.BLACK);
        return true;
    }
    
    private boolean validSponsor() {
        return CbxSponsor.getSelectedIndex() != -1;
    }
    
    private boolean validAmount() {
        Integer val = (Integer) SpnAmount.getValue();
        if (val == null) SpnAmount.setValue(0);
        else if (val < 0) SpnAmount.setValue(val * -1);
        return true;
    }
    
    private boolean validateInputs() {
        var res = validPrice() && validSponsor() && validAmount();
        BtnSave.setEnabled(res);
        return res;
    }
    
    private void saveRecord() {
        if (!validateInputs()) return;
        var index = CbxLocation.getSelectedIndex();
        var location = DB.Localities.get(index);
        if (CbxSponsor.isEnabled()){
            index = CbxSponsor.getSelectedIndex();
            var sponsor = DB.Sponsors.get(index);
            DB.Tickets.add(new Ticket(
                (Integer) SpnAmount.getValue(),
                new Ticket.Description(
                    location, Valid.Int(TxtPrice), sponsor
                )
            ));
        } else {
            var ticket = DQ.Tickets.find((row) -> row.desc.location().equals(location));
            ticket.amount = (Integer) SpnAmount.getValue();
        }
        getCurrentState();
    }
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton ActBtnClose;
    private javax.swing.JButton ActBtnLogout;
    private javax.swing.JButton BtnSave;
    private javax.swing.JComboBox<String> CbxLocation;
    private javax.swing.JComboBox<String> CbxSponsor;
    private javax.swing.JPanel Form;
    private javax.swing.JPanel Shadow;
    private javax.swing.JSpinner SpnAmount;
    private javax.swing.JButton SystemReports;
    private javax.swing.JTextField TxtPrice;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    // End of variables declaration//GEN-END:variables
}
