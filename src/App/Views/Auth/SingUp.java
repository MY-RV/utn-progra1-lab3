/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package App.Views.Auth;

import App.Database.DB;
import App.Database.DQ;
import App.Models.User;
import App.Router;
import Utilities.MYRV;
import Utilities.Valid;
import static Utilities.MYRV.MFrame.serve;
import java.awt.Color;
import java.util.Objects;
import javax.swing.JOptionPane;

/**
 *
 * @author MYRV
 */
public class SingUp extends MYRV.MFrame {

    public static void main(String[] args) { serve(new SingIn()); }
    public SingUp() {
        super(); initComponents();
        superPostInitialize();
        BasePanel.setOpaque(false);
        BasePanel.setBorder(new FrameBorder(new Color(255, 255, 255), 400, 20f));
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        RbtUserGender = new javax.swing.ButtonGroup();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        BtnRegister = new javax.swing.JButton();
        BasePanel = new javax.swing.JPanel();
        TxtUserId = new javax.swing.JTextField();
        TxtUserName = new javax.swing.JTextField();
        TxtUserPass = new javax.swing.JPasswordField();
        RbtUserGenderF = new javax.swing.JRadioButton();
        RbtUserGenderM = new javax.swing.JRadioButton();
        jLabel5 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        ActBtnClose = new javax.swing.JButton();
        ActBtnBack = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setPreferredSize(new java.awt.Dimension(400, 500));
        setResizable(false);

        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        BtnRegister.setBackground(new java.awt.Color(255, 254, 255));
        BtnRegister.setFont(new java.awt.Font("Comic Sans MS", 1, 18)); // NOI18N
        BtnRegister.setText("Registrarse");
        BtnRegister.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 204), 1, true));
        BtnRegister.setMargin(new java.awt.Insets(0, 0, 0, 0));
        BtnRegister.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnRegisterActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel3Layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(BtnRegister, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel3Layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(BtnRegister, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        jPanel2.add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 400, 400, 100));

        BasePanel.setBackground(new java.awt.Color(255, 255, 255));
        BasePanel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        TxtUserId.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        BasePanel.add(TxtUserId, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 120, 140, 30));

        TxtUserName.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        BasePanel.add(TxtUserName, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 180, 140, 30));

        TxtUserPass.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        BasePanel.add(TxtUserPass, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 240, 140, 30));

        RbtUserGenderF.setBackground(new java.awt.Color(255, 255, 255));
        RbtUserGender.add(RbtUserGenderF);
        RbtUserGenderF.setFont(new java.awt.Font("Comic Sans MS", 0, 18)); // NOI18N
        RbtUserGenderF.setText("Femenino");
        BasePanel.add(RbtUserGenderF, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 320, -1, -1));

        RbtUserGenderM.setBackground(new java.awt.Color(255, 255, 255));
        RbtUserGender.add(RbtUserGenderM);
        RbtUserGenderM.setFont(new java.awt.Font("Comic Sans MS", 0, 18)); // NOI18N
        RbtUserGenderM.setText("Masculino");
        BasePanel.add(RbtUserGenderM, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 290, -1, -1));

        jLabel5.setFont(new java.awt.Font("Comic Sans MS", 1, 18)); // NOI18N
        jLabel5.setText("Genero");
        BasePanel.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 300, 100, 40));

        jLabel7.setFont(new java.awt.Font("Comic Sans MS", 1, 18)); // NOI18N
        jLabel7.setText("Nombre");
        BasePanel.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 180, -1, -1));

        jLabel8.setFont(new java.awt.Font("Comic Sans MS", 1, 18)); // NOI18N
        jLabel8.setText("Contraseña");
        BasePanel.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 240, 100, 30));

        jLabel9.setFont(new java.awt.Font("Comic Sans MS", 1, 18)); // NOI18N
        jLabel9.setText("Identidicación");
        BasePanel.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 120, -1, -1));

        jLabel6.setFont(new java.awt.Font("Comic Sans MS", 1, 24)); // NOI18N
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setText("REGISTRO DE USUARIOS");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, 328, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, 48, Short.MAX_VALUE)
                .addContainerGap())
        );

        BasePanel.add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 30, 340, 60));

        jPanel2.add(BasePanel, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 30, 340, 470));

        ActBtnClose.setBackground(new java.awt.Color(255, 51, 51));
        ActBtnClose.setFont(new java.awt.Font("Font Awesome 6 Free Solid", 0, 20)); // NOI18N
        ActBtnClose.setForeground(new java.awt.Color(255, 255, 255));
        ActBtnClose.setText("\uf00d");
        ActBtnClose.setBorderPainted(false);
        ActBtnClose.setContentAreaFilled(false);
        ActBtnClose.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        ActBtnClose.setFocusPainted(false);
        ActBtnClose.setMargin(new java.awt.Insets(0, 0, 0, 0));
        ActBtnClose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ActBtnCloseActionPerformed(evt);
            }
        });
        jPanel2.add(ActBtnClose, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 0, 30, 30));

        ActBtnBack.setBackground(new java.awt.Color(255, 51, 51));
        ActBtnBack.setFont(new java.awt.Font("Font Awesome 6 Free Solid", 0, 20)); // NOI18N
        ActBtnBack.setForeground(new java.awt.Color(255, 255, 255));
        ActBtnBack.setText("\uf060");
        ActBtnBack.setBorderPainted(false);
        ActBtnBack.setContentAreaFilled(false);
        ActBtnBack.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        ActBtnBack.setFocusPainted(false);
        ActBtnBack.setMargin(new java.awt.Insets(0, 0, 0, 0));
        ActBtnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ActBtnBackActionPerformed(evt);
            }
        });
        jPanel2.add(ActBtnBack, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 30, 30));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Graphics/bgPurple.jpg"))); // NOI18N
        jPanel2.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 400, -1));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void ActBtnCloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ActBtnCloseActionPerformed
        System.exit(0);
    }//GEN-LAST:event_ActBtnCloseActionPerformed

    private void ActBtnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ActBtnBackActionPerformed
        resetFields();
        Router.goTo("Auth.SingIn");
    }//GEN-LAST:event_ActBtnBackActionPerformed

    private void BtnRegisterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnRegisterActionPerformed
        this.registerNewUser();
    }//GEN-LAST:event_BtnRegisterActionPerformed

    
    private void registerNewUser() {
        var id = Valid.Int(TxtUserId);
        if (id == null) {
            JOptionPane.showMessageDialog(null, "La cédula de solo debe de tener Números", "Error", JOptionPane.WARNING_MESSAGE);
            return;
        }
        if (DQ.Users.find((row) -> Objects.equals(row.id(), id)) != null) {
            JOptionPane.showMessageDialog(null, "La cédula ya ha sido registrada", "Error", JOptionPane.WARNING_MESSAGE);
            return;
        }
        var name = TxtUserName.getText();
        if (name.length() < 4) {
            JOptionPane.showMessageDialog(null, "El nombre de usuario debe de tener como minimo 4 letras", "Error", JOptionPane.WARNING_MESSAGE);
            return;
        }
        var password = TxtUserPass.getText();
        if (password.length() < 8) {
            JOptionPane.showMessageDialog(null, "La contraseña debe de tener como minimo 8 caracteres", "Error", JOptionPane.WARNING_MESSAGE);
            return;
        }
        String gender = null;
        if (RbtUserGenderF.isSelected()) gender = "Femenino";
        if (RbtUserGenderM.isSelected()) gender = "Masculino";
        if (gender == null) {
            JOptionPane.showMessageDialog(null, "Debes de seleccionar un valor para el genero", "Error", JOptionPane.WARNING_MESSAGE);
            return;
        }
        DB.Users.add(new User(id, password, name, gender, "user"));
        JOptionPane.showMessageDialog(null, "Usuario registrado con exito");
        resetFields();
        Router.goTo("Auth.SingIn");
    }
    
    private void resetFields() {
        RbtUserGenderF.setSelected(false);
        RbtUserGenderM.setSelected(false);
        TxtUserId.setText("");
        TxtUserName.setText("");
        TxtUserPass.setText("");
    }
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton ActBtnBack;
    private javax.swing.JButton ActBtnClose;
    private javax.swing.JPanel BasePanel;
    private javax.swing.JButton BtnRegister;
    private javax.swing.ButtonGroup RbtUserGender;
    private javax.swing.JRadioButton RbtUserGenderF;
    private javax.swing.JRadioButton RbtUserGenderM;
    private javax.swing.JTextField TxtUserId;
    private javax.swing.JTextField TxtUserName;
    private javax.swing.JPasswordField TxtUserPass;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    // End of variables declaration//GEN-END:variables
}
