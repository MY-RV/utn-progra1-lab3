/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package App.Views.Auth;

import App.Database.DB;
import App.Database.DQ;
import App.Router;
import Utilities.MYRV;
import Utilities.Valid;
import java.awt.Color;
import java.util.Objects;
import javax.swing.JOptionPane;


/**
 *
 * @author MYRV
 */
public class SingIn extends MYRV.MFrame {

    public static void main(String[] args) { serve(new SingIn()); }
    public SingIn() {
        super(); initComponents();
        superPostInitialize();
        FormPanel.setBorder(new FrameBorder(Color.WHITE, 400, 20f));
        Deco1.setOpaque(false); Deco2.setOpaque(false); Deco3.setOpaque(false);
        Deco1.setBorder(new FrameBorder(new Color(10, 10, 10), 40, 20f));
        Deco2.setBorder(new FrameBorder(new Color(20, 20, 20), 40, 20f));
        Deco3.setBorder(new FrameBorder(new Color(30, 30, 30), 40, 20f));
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bgPanel = new javax.swing.JPanel();
        ActBtnClose = new javax.swing.JButton();
        FormPanel = new javax.swing.JPanel();
        LblTitle = new javax.swing.JLabel();
        TxtUserId = new javax.swing.JTextField();
        jSeparator1 = new javax.swing.JSeparator();
        registrar = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        TxtPassword = new javax.swing.JPasswordField();
        BtnRegistrarse = new javax.swing.JButton();
        LblPassword = new javax.swing.JLabel();
        BtnIngresar = new javax.swing.JButton();
        LblTitle2 = new javax.swing.JLabel();
        Deco1 = new javax.swing.JPanel();
        Deco2 = new javax.swing.JPanel();
        Deco3 = new javax.swing.JPanel();
        Estudiante = new javax.swing.JLabel();
        LblTitle3 = new javax.swing.JLabel();
        LblEnunciado = new javax.swing.JLabel();
        LblTitle1 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        bgPanel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        ActBtnClose.setBackground(new java.awt.Color(255, 51, 51));
        ActBtnClose.setFont(new java.awt.Font("Font Awesome 6 Free Solid", 0, 20)); // NOI18N
        ActBtnClose.setForeground(new java.awt.Color(255, 255, 255));
        ActBtnClose.setText("\uf00d");
        ActBtnClose.setBorderPainted(false);
        ActBtnClose.setContentAreaFilled(false);
        ActBtnClose.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        ActBtnClose.setFocusPainted(false);
        ActBtnClose.setMargin(new java.awt.Insets(0, 0, 0, 0));
        ActBtnClose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ActBtnCloseActionPerformed(evt);
            }
        });
        bgPanel.add(ActBtnClose, new org.netbeans.lib.awtextra.AbsoluteConstraints(770, 0, 30, 30));

        FormPanel.setBackground(new java.awt.Color(255, 255, 255));
        FormPanel.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(255, 255, 255), 20, true));
        FormPanel.setOpaque(false);
        FormPanel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        LblTitle.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        LblTitle.setForeground(new java.awt.Color(153, 153, 153));
        LblTitle.setText("USUARIO");
        FormPanel.add(LblTitle, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 120, -1, -1));

        TxtUserId.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        TxtUserId.setBorder(null);
        FormPanel.add(TxtUserId, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 140, 260, 30));

        jSeparator1.setBackground(new java.awt.Color(102, 102, 102));
        jSeparator1.setForeground(new java.awt.Color(153, 153, 153));
        jSeparator1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        FormPanel.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 170, 260, 2));

        registrar.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        registrar.setForeground(new java.awt.Color(204, 204, 204));
        registrar.setText("¿No tinenes cuenta? intenta ");
        FormPanel.add(registrar, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 400, -1, -1));

        jSeparator2.setBackground(new java.awt.Color(102, 102, 102));
        jSeparator2.setForeground(new java.awt.Color(153, 153, 153));
        jSeparator2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        FormPanel.add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 260, 260, 2));

        TxtPassword.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        TxtPassword.setForeground(new java.awt.Color(153, 153, 153));
        TxtPassword.setBorder(null);
        FormPanel.add(TxtPassword, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 230, 260, 30));

        BtnRegistrarse.setBackground(new java.awt.Color(254, 254, 254));
        BtnRegistrarse.setFont(new java.awt.Font("Comic Sans MS", 1, 12)); // NOI18N
        BtnRegistrarse.setForeground(new java.awt.Color(46, 5, 125));
        BtnRegistrarse.setText("Registrarte");
        BtnRegistrarse.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(255, 255, 255), 1, true));
        BtnRegistrarse.setBorderPainted(false);
        BtnRegistrarse.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        BtnRegistrarse.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnRegistrarseActionPerformed(evt);
            }
        });
        FormPanel.add(BtnRegistrarse, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 400, 70, 22));

        LblPassword.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        LblPassword.setForeground(new java.awt.Color(153, 153, 153));
        LblPassword.setText("CONTRASEÑA");
        FormPanel.add(LblPassword, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 210, -1, -1));

        BtnIngresar.setBackground(new java.awt.Color(255, 254, 255));
        BtnIngresar.setFont(new java.awt.Font("Comic Sans MS", 0, 18)); // NOI18N
        BtnIngresar.setForeground(new java.awt.Color(51, 51, 51));
        BtnIngresar.setText("INGRESAR");
        BtnIngresar.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 204), 1, true));
        BtnIngresar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        BtnIngresar.setFocusable(false);
        BtnIngresar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnIngresarActionPerformed(evt);
            }
        });
        FormPanel.add(BtnIngresar, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 310, 120, 40));

        LblTitle2.setFont(new java.awt.Font("Comic Sans MS", 1, 24)); // NOI18N
        LblTitle2.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        LblTitle2.setText("INICIAR SECIÓN");
        FormPanel.add(LblTitle2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 50, 250, -1));

        bgPanel.add(FormPanel, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 30, 320, 440));

        Deco1.setBackground(new java.awt.Color(204, 204, 204));

        javax.swing.GroupLayout Deco1Layout = new javax.swing.GroupLayout(Deco1);
        Deco1.setLayout(Deco1Layout);
        Deco1Layout.setHorizontalGroup(
            Deco1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 260, Short.MAX_VALUE)
        );
        Deco1Layout.setVerticalGroup(
            Deco1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 460, Short.MAX_VALUE)
        );

        bgPanel.add(Deco1, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 20, 260, 460));

        Deco2.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout Deco2Layout = new javax.swing.GroupLayout(Deco2);
        Deco2.setLayout(Deco2Layout);
        Deco2Layout.setHorizontalGroup(
            Deco2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 200, Short.MAX_VALUE)
        );
        Deco2Layout.setVerticalGroup(
            Deco2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 480, Short.MAX_VALUE)
        );

        bgPanel.add(Deco2, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 10, 200, 480));

        Deco3.setBackground(new java.awt.Color(204, 204, 204));

        javax.swing.GroupLayout Deco3Layout = new javax.swing.GroupLayout(Deco3);
        Deco3.setLayout(Deco3Layout);
        Deco3Layout.setHorizontalGroup(
            Deco3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 140, Short.MAX_VALUE)
        );
        Deco3Layout.setVerticalGroup(
            Deco3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 500, Short.MAX_VALUE)
        );

        bgPanel.add(Deco3, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 0, 140, 500));

        Estudiante.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Estudiante.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Graphics/Estudiante.png"))); // NOI18N
        bgPanel.add(Estudiante, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 70, 450, 180));

        LblTitle3.setFont(new java.awt.Font("Comic Sans MS", 1, 24)); // NOI18N
        LblTitle3.setForeground(new java.awt.Color(255, 255, 255));
        LblTitle3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblTitle3.setText("UTN ISW PI");
        bgPanel.add(LblTitle3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 330, 450, -1));

        LblEnunciado.setBackground(new java.awt.Color(255, 255, 255));
        LblEnunciado.setFont(new java.awt.Font("Comic Sans MS", 0, 12)); // NOI18N
        LblEnunciado.setForeground(new java.awt.Color(204, 204, 204));
        LblEnunciado.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblEnunciado.setText("2022 LABIII");
        bgPanel.add(LblEnunciado, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 360, 450, -1));

        LblTitle1.setFont(new java.awt.Font("Comic Sans MS", 1, 36)); // NOI18N
        LblTitle1.setForeground(new java.awt.Color(255, 255, 255));
        LblTitle1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblTitle1.setText("MYorseth RetanaV");
        LblTitle1.setToolTipText("Minor Yorseth Retana Vásquez");
        bgPanel.add(LblTitle1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 260, 450, -1));

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Graphics/bgPurple.jpg"))); // NOI18N
        jLabel1.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabel1.setIconTextGap(0);
        bgPanel.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(-90, 0, 890, -1));

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Graphics/bgPurple.jpg"))); // NOI18N
        jLabel2.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabel2.setIconTextGap(0);
        bgPanel.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 0, 530, 500));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(bgPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(bgPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void ActBtnCloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ActBtnCloseActionPerformed
        System.exit(0);
    }//GEN-LAST:event_ActBtnCloseActionPerformed

    private void BtnIngresarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnIngresarActionPerformed
        loginUser();
    }//GEN-LAST:event_BtnIngresarActionPerformed

    private void BtnRegistrarseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnRegistrarseActionPerformed
        TxtPassword.setText("");
        Router.goTo("Auth.SingUp");
    }//GEN-LAST:event_BtnRegistrarseActionPerformed

    private void loginUser() {
        var id = Valid.Int(TxtUserId);
        if (id == null) {
            JOptionPane.showMessageDialog(null, "La cédula de solo debe de tener Números", "Error", JOptionPane.WARNING_MESSAGE);
            return;
        }
        var user = DQ.Users.find((row) -> Objects.equals(row.id(), id));
        if (user == null) {
            JOptionPane.showMessageDialog(null, "Usuario o contraseña Invalidos", "Error", JOptionPane.WARNING_MESSAGE);
            return;
        }
        var password = TxtPassword.getText();
        if (!password.equals(user.password())) {
            JOptionPane.showMessageDialog(null, "Usuario o contraseña Invalidos", "Error", JOptionPane.WARNING_MESSAGE);
            return;
        }
        DB.Auth = user;
        if ("admin".equals(user.role())) Router.goTo("TicketsForm");
        else Router.goTo("TicketsBuyer");
    }
    
    
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton ActBtnClose;
    private javax.swing.JButton BtnIngresar;
    private javax.swing.JButton BtnRegistrarse;
    private javax.swing.JPanel Deco1;
    private javax.swing.JPanel Deco2;
    private javax.swing.JPanel Deco3;
    private javax.swing.JLabel Estudiante;
    private javax.swing.JPanel FormPanel;
    private javax.swing.JLabel LblEnunciado;
    private javax.swing.JLabel LblPassword;
    private javax.swing.JLabel LblTitle;
    private javax.swing.JLabel LblTitle1;
    private javax.swing.JLabel LblTitle2;
    private javax.swing.JLabel LblTitle3;
    private javax.swing.JPasswordField TxtPassword;
    private javax.swing.JTextField TxtUserId;
    private javax.swing.JPanel bgPanel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JLabel registrar;
    // End of variables declaration//GEN-END:variables
    
}
