/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package App.Views;

import App.Database.DB;
import App.Database.DQ;
import App.Models.UserTickets;
import App.Router;
import Utilities.MYRV;
import Utilities.Valid;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.SpinnerNumberModel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author MYRV
 */
public class TicketsBuyer extends MYRV.MFrame {

    private ArrayList<UserTickets> tickets;
    private Integer Maximum = 0;
    private Integer CurrentPrice = 0;
    private boolean Loaded = false;
    
    public static void main(String[] args) { serve(new TicketsBuyer()); }
    public TicketsBuyer() {
        super(); initComponents();
        superPostInitialize();
    }
    
    @Override 
    public void setVisible(boolean visible) {
        var auth = DB.Auth;
        this.Loaded = false;
        setDefaults();
        if (visible) {
            if (auth==null ? false : ! "user".equals(auth.role())) {
                Router.goTo("Auth.SingIn");
            } else visible = setUp();
        }        
        super.setVisible(visible);
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bgPanel = new javax.swing.JPanel();
        ActBtnLogout = new javax.swing.JButton();
        ActBtnClose = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        BtnComprar = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        TblStats = new javax.swing.JTable();
        jScrollPane1 = new javax.swing.JScrollPane();
        TblBuys = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        SpnAmount = new javax.swing.JSpinner();
        CbxLocations = new javax.swing.JComboBox<>();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        LblTotalPrice = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        LblPrice = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        bgPanel.setBackground(new java.awt.Color(255, 255, 255));
        bgPanel.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        bgPanel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        ActBtnLogout.setBackground(new java.awt.Color(255, 51, 51));
        ActBtnLogout.setFont(new java.awt.Font("Font Awesome 6 Free Solid", 0, 20)); // NOI18N
        ActBtnLogout.setForeground(new java.awt.Color(255, 255, 255));
        ActBtnLogout.setText("\uf08b");
        ActBtnLogout.setBorderPainted(false);
        ActBtnLogout.setContentAreaFilled(false);
        ActBtnLogout.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        ActBtnLogout.setFocusPainted(false);
        ActBtnLogout.setMargin(new java.awt.Insets(0, 0, 0, 0));
        ActBtnLogout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ActBtnLogoutActionPerformed(evt);
            }
        });
        bgPanel.add(ActBtnLogout, new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 0, 30, 30));

        ActBtnClose.setBackground(new java.awt.Color(255, 51, 51));
        ActBtnClose.setFont(new java.awt.Font("Font Awesome 6 Free Solid", 0, 20)); // NOI18N
        ActBtnClose.setForeground(new java.awt.Color(255, 255, 255));
        ActBtnClose.setText("\uf00d");
        ActBtnClose.setBorderPainted(false);
        ActBtnClose.setContentAreaFilled(false);
        ActBtnClose.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        ActBtnClose.setFocusPainted(false);
        ActBtnClose.setMargin(new java.awt.Insets(0, 0, 0, 0));
        ActBtnClose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ActBtnCloseActionPerformed(evt);
            }
        });
        bgPanel.add(ActBtnClose, new org.netbeans.lib.awtextra.AbsoluteConstraints(770, 0, 30, 30));

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Graphics/bgPurple.jpg"))); // NOI18N
        bgPanel.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(-170, 0, 190, 500));

        BtnComprar.setBackground(new java.awt.Color(255, 254, 255));
        BtnComprar.setFont(new java.awt.Font("Comic Sans MS", 1, 18)); // NOI18N
        BtnComprar.setText("Comprar");
        BtnComprar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnComprarActionPerformed(evt);
            }
        });
        bgPanel.add(BtnComprar, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 400, 120, 40));

        TblStats.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        TblStats.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null}
            },
            new String [] {
                "Cantidad Total", "Total Pagado", "Ultima Compra"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Integer.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jScrollPane2.setViewportView(TblStats);

        bgPanel.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 420, 490, 40));

        TblBuys.setBackground(new java.awt.Color(254, 254, 254));
        TblBuys.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        TblBuys.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Locación", "Cantidad", "Precio", "Pagado", "Cromprado"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jScrollPane1.setViewportView(TblBuys);

        bgPanel.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 40, 490, 380));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Graphics/bgPurple.jpg"))); // NOI18N
        bgPanel.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 0, 490, 500));

        SpnAmount.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        SpnAmount.setModel(new javax.swing.SpinnerNumberModel(0, 0, null, 1));
        SpnAmount.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                SpnAmountStateChanged(evt);
            }
        });
        SpnAmount.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                SpnAmountKeyPressed(evt);
            }
        });
        bgPanel.add(SpnAmount, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 250, 190, 30));

        CbxLocations.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        CbxLocations.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CbxLocationsActionPerformed(evt);
            }
        });
        bgPanel.add(CbxLocations, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 90, 190, 30));

        jLabel3.setFont(new java.awt.Font("Comic Sans MS", 1, 18)); // NOI18N
        jLabel3.setText("Total a Pagar");
        bgPanel.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 300, -1, -1));

        jLabel5.setFont(new java.awt.Font("Comic Sans MS", 1, 18)); // NOI18N
        jLabel5.setText("Localización");
        bgPanel.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 60, -1, -1));

        jLabel7.setFont(new java.awt.Font("Comic Sans MS", 1, 18)); // NOI18N
        jLabel7.setText("Precio Unitario");
        bgPanel.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 140, -1, -1));

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 204), 1, true));

        jLabel8.setFont(new java.awt.Font("Font Awesome 6 Free Solid", 0, 18)); // NOI18N
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel8.setText("\ue140");

        LblTotalPrice.setFont(new java.awt.Font("Comic Sans MS", 0, 18)); // NOI18N
        LblTotalPrice.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        LblTotalPrice.setText("000");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(LblTotalPrice, javax.swing.GroupLayout.DEFAULT_SIZE, 140, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(LblTotalPrice)))
        );

        bgPanel.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 330, 190, 30));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 204), 1, true));

        jLabel6.setFont(new java.awt.Font("Font Awesome 6 Free Solid", 0, 18)); // NOI18N
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setText("\ue140");

        LblPrice.setFont(new java.awt.Font("Comic Sans MS", 0, 18)); // NOI18N
        LblPrice.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        LblPrice.setText("000");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(LblPrice, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(LblPrice, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        bgPanel.add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 170, 190, 30));

        jLabel9.setFont(new java.awt.Font("Comic Sans MS", 1, 18)); // NOI18N
        jLabel9.setText("Precio Unitario");
        bgPanel.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 220, -1, -1));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(bgPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(bgPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 500, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void ActBtnCloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ActBtnCloseActionPerformed
        System.exit(0);
    }//GEN-LAST:event_ActBtnCloseActionPerformed

    private void ActBtnLogoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ActBtnLogoutActionPerformed
        DB.Auth = null;
        Router.goTo("Auth.SingIn");
    }//GEN-LAST:event_ActBtnLogoutActionPerformed

    private void CbxLocationsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CbxLocationsActionPerformed
        getLocationInfo();
    }//GEN-LAST:event_CbxLocationsActionPerformed

    private void SpnAmountKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_SpnAmountKeyPressed
        Validate();
    }//GEN-LAST:event_SpnAmountKeyPressed

    private void BtnComprarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnComprarActionPerformed
        BuyTickets();
    }//GEN-LAST:event_BtnComprarActionPerformed

    private void SpnAmountStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_SpnAmountStateChanged
        Validate();
    }//GEN-LAST:event_SpnAmountStateChanged

    private void setDefaults() {
        this.CurrentPrice = 0;
        LblPrice.setText("---");
        LblTotalPrice.setText("---");
        SpnAmount.setValue(0);
        SpnAmount.setEnabled(false);
        BtnComprar.setEnabled(false);
        CbxLocations.removeAllItems();
    }
    
    private boolean setUp() {
        this.Loaded = false;
        setDefaults();
        getClearedtable(TblStats);
        getClearedtable(TblBuys);
        
        CbxLocations.removeAllItems();
        if (DQ.Tickets.where((row) -> row.amount > 0).isEmpty()) {
            JOptionPane.showMessageDialog(null, 
                "Actualmente no hay Tickets disponibles.\r\nVuelve más tarde!", 
                "Alerta", JOptionPane.WARNING_MESSAGE
            );
        }
        for (var row : DB.Localities) CbxLocations.addItem(row);
        CbxLocations.setSelectedIndex(-1);
        
        this.tickets = DQ.UsersTickets.where((row) -> Objects.equals(row.user_id(), DB.Auth.id()));
        refreshTable();
        refreshStats();
        this.Loaded = true;
        return this.Loaded;
    }
    
    private void refreshTable() {
        var model = getClearedtable(TblBuys);
        for (var row : tickets) {
            var ticket = row.ticket();
            model.addRow(new Object[]{
                row.location(),
                row.amount(),
                ticket.desc.price(),
                row.amount() * ticket.desc.price(),
                row.date(),
            });
        }
    }
    
    private void refreshStats() {
        var model = getClearedtable(TblStats);
        int totalAmount = 0, totalPaid = 0;
        var lastBought = "---";
        for (var row : tickets) {
            var amount = row.amount();
            var paid = amount * row.ticket().desc.price();
            totalAmount += amount;
            totalPaid += paid;
            lastBought = row.date();
        }
        model.addRow(new Object[]{
            totalAmount,
            totalPaid,
            lastBought,
        });
    }
    
    private DefaultTableModel getClearedtable(JTable table) {
        var model = (DefaultTableModel) table.getModel(); 
        int rows = model.getRowCount(); 
        for(int i = rows - 1; i >=0; i--) model.removeRow(i);
        return model;
    }

    private void getLocationInfo() {
        try {
            var index = CbxLocations.getSelectedIndex();
            if (!this.Loaded || index == -1) return;
            var location = DB.Localities.get(index);
            var ticket = DQ.Tickets.find((row) -> row.desc.location().equals(location));
            this.Maximum = ticket.amount;

            SpnAmount.setValue(0);
            SpnAmount.setEnabled(true);
            var model = new SpinnerNumberModel();
            model.setMaximum(0);
            model.setMaximum(this.Maximum);
            SpnAmount.setModel(model);
            this.CurrentPrice = ticket.desc.price();
            LblPrice.setText("" + this.CurrentPrice);
            LblTotalPrice.setText(" 0");
            BtnComprar.setEnabled(false);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, 
                "No hay tickets disponibles en esta zona", 
                "Alerta", JOptionPane.WARNING_MESSAGE
            );
        }
        
    }
    
    private void Validate() {
        var value = (Integer) SpnAmount.getValue();
        if (value > Maximum) {
            SpnAmount.setValue(Maximum);
        }
        LblTotalPrice.setText("" + (this.CurrentPrice * value));
        
        BtnComprar.setEnabled(value <= Maximum);
    }
    
    private void BuyTickets() {
        var index = CbxLocations.getSelectedIndex();
        if (!this.Loaded || index == -1) return;
        var location = DB.Localities.get(index);
        var ticket = DQ.Tickets.getIndex((row) -> row.desc.location().equals(location));
        var value = (Integer) SpnAmount.getValue();
        
        DB.Tickets.get(ticket).amount -= value;
        DB.UsersTickets.add(new UserTickets(
            DB.Auth.id(),
            location,
            value,
            new Date()
        ));
        setUp();
    }
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton ActBtnClose;
    private javax.swing.JButton ActBtnLogout;
    private javax.swing.JButton BtnComprar;
    private javax.swing.JComboBox<String> CbxLocations;
    private javax.swing.JLabel LblPrice;
    private javax.swing.JLabel LblTotalPrice;
    private javax.swing.JSpinner SpnAmount;
    private javax.swing.JTable TblBuys;
    private javax.swing.JTable TblStats;
    private javax.swing.JPanel bgPanel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    // End of variables declaration//GEN-END:variables
}
