/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */

package App.Views;

import App.Database.DB;
import App.Database.DQ;
import App.Models.UserTickets;
import App.Models.User;
import App.Router;
import Utilities.MYRV;
import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author MYRV
 */
public class Reports extends MYRV.MFrame {

    private boolean Loaded = false;
    private ArrayList<User> users = new ArrayList<>(); 
    
    //public static void main(String[] args) { serve(new Reports()); }
    public Reports() {
        super(); initComponents();
        superPostInitialize();
        UsrReport.setOpaque(false);
        UsrReport.setBorder(new FrameBorder(new Color(240, 240, 240), 400, 20f));
        LocReport.setOpaque(false);
        LocReport.setBorder(new FrameBorder(new Color(220, 220, 220), 400, 20f));
    }
    
    @Override 
    public void setVisible(boolean visible) {
        this.Loaded = false;
        resetUsrFields();
        resetLclFields();
        if (visible) setUp();
        super.setVisible(visible);
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        ActBtnClose = new javax.swing.JButton();
        ActBtnLogout = new javax.swing.JButton();
        LocReport = new javax.swing.JPanel();
        CbxLocalidades = new javax.swing.JComboBox<>();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        LblWoman = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        LblMan = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        UsrReport = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        TblBuys = new javax.swing.JTable();
        CbxUserIds = new javax.swing.JComboBox<>();
        LblUserInfo = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(204, 204, 255));
        jPanel1.setForeground(new java.awt.Color(204, 204, 255));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        ActBtnClose.setBackground(new java.awt.Color(255, 51, 51));
        ActBtnClose.setFont(new java.awt.Font("Font Awesome 6 Free Solid", 0, 20)); // NOI18N
        ActBtnClose.setForeground(new java.awt.Color(255, 255, 255));
        ActBtnClose.setText("\uf00d");
        ActBtnClose.setBorderPainted(false);
        ActBtnClose.setContentAreaFilled(false);
        ActBtnClose.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        ActBtnClose.setFocusPainted(false);
        ActBtnClose.setMargin(new java.awt.Insets(0, 0, 0, 0));
        ActBtnClose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ActBtnCloseActionPerformed(evt);
            }
        });
        jPanel1.add(ActBtnClose, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 0, 30, 30));

        ActBtnLogout.setBackground(new java.awt.Color(255, 51, 51));
        ActBtnLogout.setFont(new java.awt.Font("Font Awesome 6 Free Solid", 0, 20)); // NOI18N
        ActBtnLogout.setForeground(new java.awt.Color(255, 255, 255));
        ActBtnLogout.setText("\uf08b");
        ActBtnLogout.setBorderPainted(false);
        ActBtnLogout.setContentAreaFilled(false);
        ActBtnLogout.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        ActBtnLogout.setFocusPainted(false);
        ActBtnLogout.setMargin(new java.awt.Insets(0, 0, 0, 0));
        ActBtnLogout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ActBtnLogoutActionPerformed(evt);
            }
        });
        jPanel1.add(ActBtnLogout, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 0, 30, 30));

        LocReport.setBackground(new java.awt.Color(255, 255, 255));
        LocReport.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        CbxLocalidades.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        CbxLocalidades.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CbxLocalidadesActionPerformed(evt);
            }
        });
        LocReport.add(CbxLocalidades, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 10, 110, 20));

        jLabel3.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        jLabel3.setText("Localidad");
        LocReport.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));

        jLabel4.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        jLabel4.setText("Mujeres");
        LocReport.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 100, -1, -1));

        jLabel5.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        jLabel5.setText("Cantidad de Compras por");
        LocReport.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 40, -1, -1));

        LblWoman.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        LblWoman.setText("---");
        LocReport.add(LblWoman, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 100, -1, -1));

        jLabel7.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        jLabel7.setText("Hombres");
        LocReport.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 70, -1, -1));

        LblMan.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        LblMan.setText("---");
        LocReport.add(LblMan, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 70, -1, -1));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 200, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        LocReport.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 40, 200, 20));

        jPanel1.add(LocReport, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, 200, 130));

        UsrReport.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        TblBuys.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        TblBuys.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Localidad", "Tickets", "CostoTotal", "FechaCompra"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jScrollPane1.setViewportView(TblBuys);

        UsrReport.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 60, 470, 290));

        CbxUserIds.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        CbxUserIds.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CbxUserIdsActionPerformed(evt);
            }
        });
        UsrReport.add(CbxUserIds, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 20, 100, 20));

        LblUserInfo.setFont(new java.awt.Font("Comic Sans MS", 0, 14)); // NOI18N
        LblUserInfo.setText("---");
        UsrReport.add(LblUserInfo, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 20, 210, 20));

        jLabel2.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        jLabel2.setText("Reporte de Usuario");
        UsrReport.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, -1, -1));

        jPanel1.add(UsrReport, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 100, 510, 370));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Graphics/bgPurple.jpg"))); // NOI18N
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 750, 500));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void ActBtnLogoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ActBtnLogoutActionPerformed
        DB.Auth = null;
        Router.goTo("Auth.SingIn");
    }//GEN-LAST:event_ActBtnLogoutActionPerformed

    private void ActBtnCloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ActBtnCloseActionPerformed
        Router.goTo("TicketsForm");
    }//GEN-LAST:event_ActBtnCloseActionPerformed

    private void CbxUserIdsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CbxUserIdsActionPerformed
        getUserBuys();
    }//GEN-LAST:event_CbxUserIdsActionPerformed

    private void CbxLocalidadesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CbxLocalidadesActionPerformed
        getLocalityBuys();
    }//GEN-LAST:event_CbxLocalidadesActionPerformed

    private void setUp() {
        this.Loaded = false;
        resetUsrFields();
        resetLclFields();
        
        this.users = DQ.Users.where((user) -> user.role().equals("user"));
        for (var user : users) CbxUserIds.addItem(user.id()+"");
        CbxUserIds.setSelectedIndex(-1);
        
        for (var row : DB.Localities) CbxLocalidades.addItem(row);
        CbxLocalidades.setSelectedIndex(-1);
        
        this.Loaded = true;
    }
    
    private void resetUsrFields() {
        this.getClearedtable(TblBuys);
        CbxUserIds.removeAllItems();
        LblUserInfo.setText("---");
        this.users = new ArrayList<>();
    }
    
    private void resetLclFields() {
        CbxLocalidades.removeAllItems();
        LblMan.setText("---");
        LblWoman.setText("---");
    }
    
    private DefaultTableModel getClearedtable(JTable table) {
        var model = (DefaultTableModel) table.getModel(); 
        int rows = model.getRowCount(); 
        for(int i = rows - 1; i >=0; i--) model.removeRow(i);
        return model;
    }
    
    private void getUserBuys() {
        if (!this.Loaded) return;
        var index = CbxUserIds.getSelectedIndex();
        if (index == -1) return;
        var user = this.users.get(index);
        var buys = DQ.UsersTickets.where((row) -> Objects.equals(row.user_id(), user.id()));
        var model = this.getClearedtable(TblBuys);
        for (UserTickets buy : buys) {
            var ticket = buy.ticket();
            model.addRow(new Object[]{
                ticket.desc.location(),
                buy.amount(),
                ticket.desc.price() * buy.amount(),
                buy.date()
            });
        }
        LblUserInfo.setText(user.name() + "(" + user.id() + ")");
    }
    
    private void getLocalityBuys() {
        if (!this.Loaded) return;
        var index = CbxLocalidades.getSelectedIndex();
        if (index == -1) return;
        var loacality = DB.Localities.get(index);
        var buys = DQ.UsersTickets.where((row) -> Objects.equals(row.location(), loacality));
        
        int boys = 0, girls = 0;
        HashMap<Integer, Boolean> foundUsers = new HashMap<>();
        
        for (var buy : buys) {
            if (foundUsers.get(buy.user_id()) != null) continue;
            var gender = buy.user().gender();
            if (gender.equals("Masculino"))     boys++;
            else if (gender.equals("Femenino")) girls++;
            foundUsers.put(buy.user_id(), true);
        }
        
        LblMan.setText(""+boys);
        LblWoman.setText(""+girls);
    }
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton ActBtnClose;
    private javax.swing.JButton ActBtnLogout;
    private javax.swing.JComboBox<String> CbxLocalidades;
    private javax.swing.JComboBox<String> CbxUserIds;
    private javax.swing.JLabel LblMan;
    private javax.swing.JLabel LblUserInfo;
    private javax.swing.JLabel LblWoman;
    private javax.swing.JPanel LocReport;
    private javax.swing.JTable TblBuys;
    private javax.swing.JPanel UsrReport;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables

}
